php-react-promise (3.2.0-3) unstable; urgency=medium

  * Drop test failing with PHP 8.4 (Closes: #1089781)

 -- David Prévot <taffit@debian.org>  Tue, 17 Dec 2024 11:34:52 +0100

php-react-promise (3.2.0-2) unstable; urgency=medium

  * Use --require-file for phpabtl (Closes: #1074205)

 -- David Prévot <taffit@debian.org>  Sat, 20 Jul 2024 15:02:04 +0900

php-react-promise (3.2.0-1) unstable; urgency=medium

  [ Ayesh Karunaratne ]
  * [PHP 8.4] Fixes for implicit nullability deprecation

  [ Simon Frings ]
  * Update test suite to improve PHP 8.4+ support
  * Prepare v3.2.0 release

  [ Christian Lück ]
  * Include previous exceptions when reporting unhandled promise rejections

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Mon, 10 Jun 2024 07:18:02 +0200

php-react-promise (3.1.0-1) unstable; urgency=medium

  * Upload to unstable now that Bookworm has been released

  [ Christian Lück ]
  * Describe callable arguments for Deferred
  * Describe callable arguments for Promise

  [ Simon Frings ]
  * Prepare v3.1.0 release

  [ David Prévot ]
  * Don’t expect src/ in test output

 -- David Prévot <taffit@debian.org>  Sat, 18 Nov 2023 15:34:37 +0100

php-react-promise (3.0.0-1) experimental; urgency=medium

  [ Jan Sorgalla ]
  * Introduce 3.0.0 section in changelog and add note about global queue
  * Fix PHPUnit warnings for deprecated PHPUnit_Framework_TestCase::getMock()
    (Closes: #1039824)
  * Update copyright year

  [ Christian Lück ]
  * Mark `FulfilledPromise` and `RejectedPromise` as internal
  * Prepare v3.0.0 release

 -- David Prévot <taffit@debian.org>  Sun, 16 Jul 2023 11:42:13 +0200

php-react-promise (2.10.0-1) experimental; urgency=medium

  * Upload new minor to experimental during the Freeze

  [ Simon Frings ]
  * Prepare v2.10.0 release

  [ Nicolas Hedger ]
  * feat: add support for DNF types on PHP 8.2

  [ David Prévot ]
  * Update renamed lintian tag names in lintian overrides.
  * Update standards version to 4.6.2, no changes needed.

 -- David Prévot <taffit@debian.org>  Thu, 04 May 2023 19:18:13 +0200

php-react-promise (2.9.0-3) unstable; urgency=medium

  * CI depends on pkg-php-tools for phpabtpl(1)
    Thanks to Athos Ribeiro <athos.ribeiro@canonical.com>

 -- David Prévot <taffit@debian.org>  Fri, 27 May 2022 15:30:09 +0200

php-react-promise (2.9.0-2) unstable; urgency=medium

  * Allow stderr for CI
  * Mark package as Multi-Arch: foreign

 -- David Prévot <taffit@debian.org>  Thu, 26 May 2022 19:52:50 +0200

php-react-promise (2.9.0-1) unstable; urgency=medium

  [ Christian Lück ]
  * Prepare v2.9.0 release

  [ Sam Reed ]
  * Add .gitattributes to exclude dev files from exports

  [ Simon Frings ]
  * Run tests on PHPUnit 9
  * Support PHP 8.1

  [ David Prévot ]
  * Use git source and DEP-14
  * Update copyright
  * Drop patches not needed anymore
  * Drop versioned dependencies
  * Use phpabtpl
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Restore package under the team (Closes: #1006400)
  * Update Standards-Version to 4.6.1
  * Set upstream metadata fields: Repository.
  * Remove obsolete field Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- David Prévot <taffit@debian.org>  Tue, 24 May 2022 23:26:16 +0200

php-react-promise (2.7.0-2) unstable; urgency=medium

  * Backport two upstream commits to achieve PHP 8 compatibility,
    info from aurélien Maille (Closes: #980567)
  * Usual updates to packaging
  * This release will break with PHPUnit 10 ⇒ version B-D

 -- Thorsten Glaser <tg@mirbsd.de>  Mon, 01 Feb 2021 01:26:18 +0100

php-react-promise (2.7.0-1) unstable; urgency=medium

  * New upstream version.
  * Bump Standards-Version (no changes needed).

 -- Dominik George <natureshadow@debian.org>  Sun, 02 Dec 2018 00:40:47 +0100

php-react-promise (2.5.1-1) unstable; urgency=medium

  * Take over with permission of previous team/uploader
  * New upstream version
  * Bump Policy
    - remove get-orig-source target
  * Appease lintian
    - bump debhelper compat to 11
    - use https in debian/copyright
    - contribute UMEYAGA metadata
  * Update lintian overrides
  * Facultatively switch to Teckids Debian Task Force salsa repo
  * Fix build with new PHPUnit (Closes: #882913)
  * Update Homepage, package description

 -- Thorsten Glaser <tg@mirbsd.de>  Sat, 19 May 2018 22:58:58 +0200

php-react-promise (2.4.1-1) unstable; urgency=medium

  [ Jan Sorgalla ]
  * Fix some() not cancelling pending promises when too much input
    promises reject
  * Prepare changelog for v2.4.1 release

  [ David Prévot ]
  * Update Standards-Version to 3.9.8

 -- David Prévot <taffit@debian.org>  Tue, 03 May 2016 20:39:28 -0400

php-react-promise (2.4.0-1) unstable; urgency=medium

  [ Jan Sorgalla ]
  * Reject some() (and any()) if the input array contains not enough items
  * Support foreign thenables in resolve()
  * Prepare changelog for v2.4.0 release

  [ David Prévot ]
  * Install upstream README

 -- David Prévot <taffit@debian.org>  Fri, 01 Apr 2016 16:08:42 -0400

php-react-promise (2.3.0-1) unstable; urgency=medium

  [ Jan Sorgalla ]
  * Allow cancellation of promises returned by functions working on
    promise collections
  * Prepare changelog for v2.3.0 release

  [ Josh Di Fabio ]
  * Handle \Throwable in the same way as \Exception

 -- David Prévot <taffit@debian.org>  Fri, 25 Mar 2016 03:10:02 -0400

php-react-promise (2.2.2-1) unstable; urgency=medium

  [ Jan Sorgalla ]
  * Update copyright year
  * Prepare changelog for v2.2.2 release

  [ Christian Lück ]
  * Cancellation handler must only be called once

  [ David Prévot ]
  * Update copyright (years)
  * Update Standards-Version to 3.9.7
  * Build with latest pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Thu, 03 Mar 2016 15:42:46 -0400

php-react-promise (2.2.1-1) unstable; urgency=medium

  [ Jan Sorgalla ]
  * Update copyright year
  * Update email
  * Assign result and reset handlers early when settling a promise
  * Prepare changelog for v2.2.1 release

  [ David Prévot ]
  * Update copyright

 -- David Prévot <taffit@debian.org>  Sat, 04 Jul 2015 16:02:54 -0400

php-react-promise (2.2.0-3) unstable; urgency=medium

  * Upload to unstable since it’s the end of the freeze
  * Use homemade autoload.php for tests

 -- David Prévot <taffit@debian.org>  Fri, 24 Apr 2015 14:55:48 -0400

php-react-promise (2.2.0-2) experimental; urgency=medium

  * Provide homemade autoload.php

 -- David Prévot <taffit@debian.org>  Sat, 28 Feb 2015 10:35:30 -0400

php-react-promise (2.2.0-1) experimental; urgency=medium

  * Upload to experimental to respect the freeze

  [ Jan Sorgalla ]
  * Introduce ExtendedPromiseInterface
  * Conditionally require functions.php
  * Prepare changelog for v2.2.0 release

 -- David Prévot <taffit@debian.org>  Sat, 03 Jan 2015 11:14:59 -0400

php-react-promise (2.1.0-1) unstable; urgency=medium

  [ Jan Sorgalla ]
  * Switch to PSR-4
  * Introduce CancellablePromiseInterface
  * Make CancellablePromiseInterface extend PromiseInterface
  * Prepare changelog for v2.1.0 release

  [ David Prévot ]
  * Adapt package for PSR-4 distribution
  * Use php-symfony-class-loader instead of php-symfony-classloader

 -- David Prévot <taffit@debian.org>  Sun, 19 Oct 2014 11:51:23 -0400

php-react-promise (2.0.0-1) unstable; urgency=low

  * Initial release

 -- David Prévot <taffit@debian.org>  Tue, 14 Oct 2014 16:08:10 -0400
